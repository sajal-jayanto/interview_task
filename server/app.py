from flask import Flask , request , jsonify , json 
from flask_pymongo import PyMongo
from bson.objectid import ObjectId
from flask_cors import CORS, cross_origin
from werkzeug.security import generate_password_hash , check_password_hash
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    get_jwt_identity
)
# import datetime

app = Flask(__name__)

cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

##############################################################################
app.config['JWT_SECRET_KEY'] = 'super-secret'  
jwt = JWTManager(app)

##############################################################################
app.config["MONGO_URI"] = "mongodb://localhost:27017/myDatabase"
mongo = PyMongo(app)
db = mongo.db

#############################################################################



@app.route("/register" , methods=['POST'])
def register_user():
    
    _json = request.json
    _user_name = _json['username']
    _email = _json['email']
    _password = _json['password']

    if _user_name and _email and _password:
        
        try:
            _dose_email_exists = db.users.find_one({ "email" : _email})
        except:
            return jsonify({'message' : 'internal server error'}) , 500 
        if _dose_email_exists == None:
            try :
                _user_id = db.users.insert_one({ "username" : _user_name, "email" : _email, "password" : generate_password_hash(_password , method='sha256') } ).inserted_id
                _db_response = db.users.find_one({"_id" : ObjectId(_user_id)})
                _user_info = { "username" : _db_response['username'] , "email" : _db_response['email']  }
                
                token = create_access_token(identity={'_id' : str(_user_id)})
                return jsonify({ 'data' : _user_info , 'token' : token}) , 200
            except:
                return jsonify({ 'message' : 'internal server error' }) , 500 
        else:
            return jsonify({'message' : 'Email Alrady Exists'}) , 200 
    else:
        return jsonify({ 'message' : 'Invalid Data' }) , 422

@app.route("/login" , methods=['POST'])
def login_user():

    _json = request.json
    _email = _json['email']
    _password = _json['password']

    if _email and _password:

        try:
            _find_by_emali = db.users.find_one({ "email" : _email})

            if _find_by_emali == None:
                return jsonify({'message' : 'Email does not exist'}) , 200
            else:
                _dose_password_match = check_password_hash(_find_by_emali['password'] , _password)

                if _dose_password_match == True:
                    token = create_access_token(identity={'_id' : str(_find_by_emali['_id'])})
                    return jsonify({ 'token' : token }) , 200
                else:
                    return jsonify({ 'message' : 'Wrong Pawwsord' }) , 200 
        except:
            return jsonify({ 'message' : 'internal server error' }) , 500 
    else:
        return jsonify({ 'message' : 'Invalid Data' }) , 422


@app.route("/mark-as-favorite" , methods=['POST'])
def markAsFavorite():

    _json = request.json
    _user_id = _json['user_id']
    _movie_title = _json['title']

    if _user_id:
        
        try:
            _find_by_user_id = db.favorite.find_one({ "user_id" : ObjectId(_user_id)})
            
            if _find_by_user_id == None:
                
                _user_id = db.favorite.insert_one({ "user_id" : ObjectId(_user_id) , "movies" : [str(_movie_title)] })
                return jsonify({ 'message' : 'Movie add to favorite list' }) , 200

            else:
                
                _user_id = db.favorite.update_one( { "user_id" : ObjectId(_user_id) } , { "$push" : { "movies" : str(_movie_title) } } )
                return jsonify({ 'message' : 'Movie add to favorite list' }) , 200
        except:
            return jsonify({ 'message' : 'internal server error' }) , 500
    else:
        return jsonify({ 'message' : 'Invalid Data' }) , 422


@app.route("/favorite-list" , methods=['POST'])
def getallfavorite():

    _json = request.json
    _user_id = _json['user_id']
    if _user_id:
        
        try:
            _all_favorite_list = db.favorite.find_one({ "user_id" : ObjectId(_user_id) }) 
            _movies = { "movies" : _all_favorite_list['movies'] }
            # print(_all_favorite_list['movies'])
            return jsonify({ "data" : _movies }) , 200
        except:
            return jsonify({ 'message' : 'internal server error' }) , 500
    else:
        return jsonify({ 'message' : 'Invalid Data' }) , 422
    

if __name__ == "__main__":
    app.run(port=5000, debug=True)