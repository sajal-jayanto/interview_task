import React, { useState } from 'react';
import { Modal , Row , Card , Col , Button}  from 'react-bootstrap';
import jwt from 'jsonwebtoken';
import axios from 'axios';
import 'bootstrap/dist/css/bootstrap.min.css';

const MyVerticallyCenteredModal = (props) =>{

    const [ message , setmesage ] = useState('');

    const addfavourit = (imdbID) => {
        
        const token = localStorage.getItem('token');
        const decoded = jwt.verify(token , 'super-secret');
        console.log(decoded.identity._id)

        const data = {
            user_id : decoded.identity._id,
            title : imdbID
        }

        axios.post('http://127.0.0.1:5000/mark-as-favorite' , data).then(response => {
            //console.log(response.data)
            setmesage('Movie added to you favourit list');
        }); 
    }


    return (
        <Modal
            {...props}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
        >
            <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    {props.movie.Title}
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                
                <Row>
                    <Col xs={4} md={4}> <Card.Img variant="top" src={props.movie.Poster}  width={160} height={250} /> </Col>
                    <Col xs={4} md={4}> 
                        <p>
                            Year : {props.movie.Year} 
                            <br/>
                            Releas Date : { props.movie.Released}
                            <br/>
                            Duration : {props.movie.Runtime}
                            <br/>
                            Actors : { props.movie.Actors}
                            <br/>
                            IMDB Rating : { props.movie.imdbRating}
                        </p>
                        
                    </Col>
                    <Col xs={4} md={4}>
                        <p>
                            Votes : {props.movie.imdbVotes}
                            <br/>
                            Language : {props.movie.Language}
                            <br/>
                            Country : {props.movie.Country}
                            <br/>
                            Awards : {props.movie.Awards}
                        </p>
                    </Col>
                </Row>
            </Modal.Body>
            <Modal.Footer>
                { message }
                <Button onClick={() => addfavourit(props.movie.imdbID)}>Add As Favourit</Button>
            </Modal.Footer>
        </Modal>
    );
}


export default MyVerticallyCenteredModal;

 