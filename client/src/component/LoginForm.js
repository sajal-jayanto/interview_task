import React , { useState , useEffect }from 'react';
import Layout from './Layout';
import axios from 'axios';
import { Form , Button , Card , Alert}  from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';
import { Link } from 'react-router-dom';
import { useHistory } from "react-router-dom";
 
const LoginFrom = () => {
  
    const [ email , setemail] = useState('');
    const [ password , setpassword] = useState('');
    const [ valid , setvalid ] = useState(false);
    const [errmessage , seterrmessage ] = useState('');
    const history = useHistory();

    useEffect(() => {
        if(valid === true) { setvalid(false) }
    } , [email , password]);    


    const submitButton = () =>{

        if(email === "" || password === ""){
            setvalid(true);
            return ;
        }

        const userInfo = {
            email : email, 
            password : password
        }

        axios.post('http://127.0.0.1:5000/login' , userInfo).then(response => { 

            if(response.data.token != null){
                localStorage.setItem('token' , response.data.token);
                history.push('/dashbord');
            }
            else{
                seterrmessage(response.data.message)
            }
        });
    } 

    const show = {
        register : true ,
        login : false
    }

    return (
        <>
            <Layout page={show}  />
            <div style={{paddingTop: '8%' , paddingLeft: '35%'}}>
                <Card style={{ width: '35rem' }}>
                <Card.Header> <h3>Login</h3>  </Card.Header>
                    <Card.Body>
                        {errmessage !== '' ? 
                            <Form.Group >
                                <Alert variant='danger'>
                                    {errmessage}
                                </Alert>
                            </Form.Group> : <> </>
                        }
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control type="email" placeholder="Enter email" onChange={(e) => setemail(e.target.value)} />
                        </Form.Group>

                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control type="password" placeholder="Password" onChange={(e) => setpassword(e.target.value)}/>
                        </Form.Group>
                        { 
                            valid ? 
                            <Form.Text className="text-muted">
                                Invalid Data
                            </Form.Text> : <> </>
                        }
                        <Link to="/register" > Create an account </Link>
                        <hr/> 
                        <Button variant="primary" onClick={submitButton} > Submit </Button>
                    </Card.Body>
                </Card>
            </div>
        </>
    );
};
 

 
export default LoginFrom;