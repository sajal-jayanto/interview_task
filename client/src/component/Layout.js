import React from 'react';
import { Container , Navbar , Nav}  from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css';



const Layout = (props) =>{

    console.log(props);
    return (
        <Navbar expand="lg" variant="dark" bg="dark">
            <Container>
                <Navbar.Brand>OMBD</Navbar.Brand>
            </Container>
            <Nav>
                { props.page.login ?  <Nav.Link href="/login"> Login </Nav.Link> : <> </> } 
                { props.page.register ? <Nav.Link href="/register"> Register </Nav.Link> : <> </>} 
                
            </Nav>
        </Navbar>
    );
}


export default Layout;

