import React , { useState , useEffect }from 'react';
import Layout from './Layout';
import axios from 'axios';
import { Form , Button , Card , Alert }  from 'react-bootstrap'
import { useHistory } from "react-router-dom";
import 'bootstrap/dist/css/bootstrap.min.css';

const RegisterFrom = () => {

    const [username , setusername ] = useState('');
    const [email    , setemail ] = useState('');
    const [password , setpassword] = useState('');
    const [confirmpassword , setconfirmpassword ] = useState('');
    const [valid , setvalid] = useState(false);
    const [errmessage , seterrmessage ] = useState('');
    const history = useHistory();

    useEffect(() => {
        if(valid === true) { setvalid(false) }
    } , [username , email , password , confirmpassword]);


    const submitButton = () => {
        
        if(username === "" || email === "" || password === "" || password !== confirmpassword){
            setvalid(true);
            return 
        }
        else{

            const userInfo = {
                username : username,
                email : email,
                password : password
            }
            
            axios.post('http://127.0.0.1:5000/register' , userInfo).then(response => {
               
                console.log(response.data)
                if(response.data.token != null){
                    localStorage.setItem('token' , response.data.token);
                    history.push('/dashbord');
                }
                else {
                    seterrmessage(response.data.message)
                }

            });   
        }
    }

    const show = {
        register : false ,
        login : true
    }
  
    return (
        <>
            <Layout page={show}  />
            <div style={{paddingTop: '5%' , paddingLeft: '38%'}}>
                <Card style={{ width: '35rem' }}>

                    <Card.Header> <h3>Register</h3>  </Card.Header>
                    <Card.Body>
                        {errmessage !== '' ? 
                            <Form.Group >
                                <Alert variant='danger'>
                                    {errmessage}
                                </Alert>
                            </Form.Group> : <> </>
                        }
                        <Form.Group >
                            <Form.Label>User Name</Form.Label>
                            <Form.Control placeholder="Enter User Name" onChange={(e) => setusername(e.target.value)}  />
                        </Form.Group>
                        <Form.Group >
                            <Form.Label>Email</Form.Label>
                            <Form.Control type="email" placeholder="Enter email" onChange={(e) => setemail(e.target.value)} />
                        </Form.Group>

                        <Form.Group >
                            <Form.Label>Password</Form.Label>
                            <Form.Control  type="password" placeholder="Password" onChange={(e) => setpassword(e.target.value)} required/>
                        </Form.Group>

                        <Form.Group >
                            <Form.Label>Confirm Password</Form.Label>
                            <Form.Control type="password" placeholder="Confirm Password" onChange={(e) => setconfirmpassword(e.target.value)} required/>
                        </Form.Group>
                        { 
                            valid ? 
                            <Form.Text className="text-muted">
                                Invalid Data
                            </Form.Text> : <> </>
                        }
                        <hr/> 
                        <Button variant="primary" onClick={submitButton}> Submit </Button>
                    </Card.Body>
                </Card>
            </div>
        </>
    );
};

export default RegisterFrom;