import React , { useState , useEffect } from 'react';
import Layout from './Layout';
import MyVerticallyCenteredModal from './Moviedetails'
import axios from 'axios';
import { Container , Row , Col , Card  , InputGroup , Form , Button , FormControl , Alert }  from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';

const Dashbord = () =>{

    const [ movielist , setmovielist ] = useState([]);
    const [ modalShow, setModalShow] = useState(false);
    const [ movieTitle , setmovieTitle ] = useState('');
    const [ movieYear , setmovieYear] = useState('');
    const [ selecteditem , setselecteditem] = useState('');
    const [ notfound , setnotfound ] = useState(false);

    useEffect(()  => {
        first_time();
    } , []);   

    useEffect(() => {
        setnotfound(false);
    }, [movieTitle , movieYear]);

    const first_time = async () => {
        
        const response = await axios.get('http://www.omdbapi.com' , {  params: { s: 'genre', apikey : '5c487ea4' } });

        setmovielist(response.data.Search);
        console.log(response.data.Search);
    }

    const moviedetails = async (imdbID) => {
        
        const response = await axios.get('http://www.omdbapi.com' , {  params: { i : imdbID , apikey : '5c487ea4' } });
        setselecteditem(response.data);
        setModalShow(true);
    }

    const searchbutton = async () => {

        if(movieTitle === '' && movieTitle === ''){
            setnotfound(true);
            return ;
        }
        const parm = { apikey : '5c487ea4' }
        if(movieTitle !== '') parm['t'] = movieTitle;
        if(movieYear !== '') parm['y'] = movieYear;

        const response = await axios.get('http://www.omdbapi.com' , { params: parm });

        setselecteditem(response.data);
        setModalShow(true);
    }

    const show = {
        register : false ,
        login : false
    }

    return (
        <>
            <Layout page={show} />
            <Container>
                <Card style={{margin:30 , padding : 20 }}>
                <h3 style={{textAlign:'center'}}> Search Movie </h3>
                <hr/>
                <Row>
               
                    <Col xs={3} md={3}>  </Col>
                    <Col>
                        
                        <Form.Row className="align-items-center">
                
                            <Col xs="auto">
                                <Form.Label htmlFor="inlineFormInput" srOnly>Name</Form.Label>
                                <Form.Control className="mb-2" placeholder="Title" onChange={(e) => setmovieTitle(e.target.value)}/>
                            </Col>
                            <Col xs="auto">
                                <Form.Label htmlFor="inlineFormInputGroup" srOnly>Username</Form.Label>
                                <InputGroup className="mb-2">
                                    <FormControl type="number" placeholder="Year" onChange={(e) => setmovieYear(e.target.value)} />
                                </InputGroup>
                            </Col>
    
                            <Col xs="auto">
                                <Button className="mb-2" onClick={searchbutton} >Submit</Button>
                            </Col>
                        </Form.Row>
                        {notfound ? 
                            <Form.Row className="align-items-center">
                                <Alert variant='danger'>
                                    No movie Found
                                </Alert>
                            </Form.Row> : <> </>
                        }
                    </Col>
                    
                </Row>
                </Card>
                { movielist.length > 0 ? 
                    <div style={{ display : 'flex' , justifyContent: 'center' , flexWrap : 'wrap'}}>
                        {movielist.map((movie , index) => (
                            <Card key={movie.imdbID} style={{width: 350 , margin : 10 }} onClick={() => moviedetails(movie.imdbID)}>
                                <Card.Img variant="top" src={movie.Poster}  width={160} height={200} />
                                <Card.Body>
                                    <Card.Title>{movie.Title}</Card.Title>
                                </Card.Body>
                                <Card.Footer>
                                    <small className="text-muted">Year : {movie.Year}</small>
                                </Card.Footer>
                            </Card>
                        ))}
                    </div> : <h1> loding....</h1> 
                }
            </Container>
            <MyVerticallyCenteredModal show={modalShow} movie={selecteditem} onHide={() => setModalShow(false)  }/>
        </>
    );
}


export default Dashbord;