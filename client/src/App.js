import './App.css';

import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";
  
import LoginFrom from './component/LoginForm';
import RegisterFrom from './component/RegisterFrom';
import Dashbord from './component/Dashbord';

const App = () =>{
  
    return (
        <div className="App">
            
            <Router>
                <Switch>
                    <Route exact path="/">
                        <LoginFrom/>
                    </Route>
                    <Route path="/register">
                        <RegisterFrom/>
                    </Route>
                    <Route path="/dashbord">
                        <Dashbord />
                    </Route>
                </Switch>
            </Router>

        </div>
    );
}

export default App;
