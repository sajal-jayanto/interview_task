# interview_task

### git config 

`git clone https://gitlab.com/sajal-jayanto/interview_task.git` 


# server setup
### Python version 3.9.1
### mongoDB as a Database

`cd server`

`pip install virtualenv`

`virtualenv env`

`source env/Scripts/activate`

`pip install flask , flask_pymongo , flask_cors , flask_jwt_extended`

`python app.py`

### server is running at port 5000


# client setup 

### node verison 12.14.0
### npm version 6.14.5
### yarn verison 1.22.5

`cd client`

`yarn`

`yarn start`

### Client server running at port 3000